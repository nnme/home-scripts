#!/usr/bin/python 
# _*_ coding:utf-8 _*_
__author__ = "Eugen K"
__email__ = "eugen_k69@yahoo.com"


import requests
import datetime
from StringIO import StringIO

   
"""
this is api for emaillistverify.com
check your 'pycurl' module (for install use pip or easy_install or go:
                            http://pycurl.io/docs/latest/install.html)


example for use:
for check one address:

    from emailverify import EmailListVerifyOne


    E = EmailListVerifyOne('PUT_YOUR_KEY', 'test@mail.com')
    result = E.control()
    print result // answer is string 'ok' or 'fail'

for check bulk address:

    from emailverify import EmailListVerifyBulk   

    B = EmailListVerifyBulk('PUT_YOUR_KEY', 'path/to/file.csv')
    B.upload() // you upload your file in server(ATTENTION! you do it only once, because for your file will create ID_FILE)
    B.get_info()   // here will be answer from server  
                     something like that:
                    135094|6565_clean.csv|no|7|7|finished|1456415517
                    |https://app.emaillistverify.com/app/webroot/files/4195/6565/result_ok_6565_2016-02-25.csv
                    |https://app.emaillistverify.com/app/webroot/files/4195/6565/result_all_6565_2016-02-25.csv
    

    """


class EmailListVerifyOne():
   

    def __init__(self, key, email):
        self.key = key
        self.email = email
        self.verif = "https://app.emaillistverify.com/api/verifEmail?secret="
        self.url = self.verif+self.key+"&email="+self.email

    
    def control(self):
        r = requests.get(self.url)
        return r.text


class EmailListVerifyBulk():

    def __init__(self, key, user_file):
        datenow = datetime.datetime.now()
        self.key = key
        self.name = 'File' + datenow.strftime("%Y-%m-%d %H:%M")
        self.user_file = user_file
        self.url = 'https://app.emaillistverify.com/api/verifApiFile?secret='+key+'&filename=%s' % self.name

    def upload(self):
        import pycurl
        
        infile = open('id_file', 'w')
        c = pycurl.Curl()
        c.setopt(c.POST, 1)
        c.setopt(c.URL, self.url)
        c.setopt(c.HTTPPOST, [('file_contents', (
                    c.FORM_FILE, self.user_file,
                    c.FORM_CONTENTTYPE, 'text/plain',
                    c.FORM_FILENAME, self.name.replace(' ','_'),)),])
        c.setopt(c.WRITEFUNCTION, infile.write)
        c.setopt(c.VERBOSE, 1)
        c.perform()
        c.close()


    def get_info(self):
        with open('id_file','r') as f:
            ids = f.read()
        url = 'https://app.emaillistverify.com/api/getApiFileInfo?secret='+self.key+'&id=%s' % ids
        r = requests.get(url)
        with open('result.txt', 'a') as res:
            res.write(r.content+'\n')
        print r.content


if __name__ == '__main__':
    # E = EmailListVerifyOne('dV4YFp8o4Gan3hCN3XMAb', 'test69@yahoo.com')
    # print E.control()  
    B = EmailListVerifyBulk('dV4YFp8o4Gan3hCN3XMAb', 'test.txt')
    # B.upload() #for check of example uncoment this line just one time, when your file will be upload coment this again        
    B.get_info()
    pass