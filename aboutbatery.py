#!/usr/bin/python 
# _*_ coding: utf-8 _*_

import subprocess
import time
import pyttsx
from subprocess import Popen, PIPE


class TakeInfo():
   
    def info(self):
        while True:
            print 'new cycle'
            proc = Popen('acpi', 
                         shell=True, 
                         stdout=PIPE, 
                         stderr=PIPE)
            proc.wait()
            res = proc.communicate()
            text = res[0]
            check = [x for x in range(0, 100, 5)]
            for i in check:
                if str(i)+'%' in text:
                    Mouth().speak(text)
                    break
                else:
                    print 'continue'
                    continue          



class Mouth():

    def speak(self, text):
        self.text = text
        engine = pyttsx.init()
        rate = engine.getProperty('rate')
        engine.setProperty('rate', rate-70)
        engine.say(text)
        engine.runAndWait()
        print text
        time.sleep(60)
        engine.stop()
                
            
            
if __name__ == '__main__':    
    TakeInfo().info()
